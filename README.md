# Qemu Scripts

runvnet runs the shim to allow qemu to use the macOS virtualised network without
needing root permissions.

mkcowchild creates a qcow2 image backed off given source image. It also creates
an iso with appropriate cloud-init variables.

eg.: `./mkcowchild -s <source> -h <hostname> [ -r <size> -a <ip> -g <gw> -d <dns> ]`

`runvm <qcow2> <iso>` runs the VM

To make ssh less of a faff I add
```
Host 192.168.105.*
        StrictHostKeyChecking no
```
